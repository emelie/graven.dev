# PGP Key signing policy of Emelie Graven

## Key covered by this policy
```
pub   ed25519 2021-09-27 [SC]
      16D36E7BFBB00641BBF6574D9DEBB56FE610BAD1
uid           [ultimate] Emelie Graven <emelie@graven.dev>
uid           [ultimate] Emelie Graven <emelie@graven.se>
sub   ed25519 2021-09-27 [S]
sub   cv25519 2021-09-27 [E] [expires: 2023-03-06]
sub   ed25519 2021-09-27 [A] [expires: 2023-03-06]
```

## Certification levels

I use three levels of certification when signing keys. Each certification level also encompasses the requirements of levels below it.

* Level 0 (Generic certification)
	This type of signature is used to certify that the key holder is in control of the email address of the UID in question. No assertations are made about the key holder's identity.

* Level 2 (Casual certification)
	This type of signature is used to certify that identity of the key holder has been verified with at least one form of government-issued photo ID, or another equally secure means of identification.

* Level 3 (Positive certification)
	This type of signature is used to certify that the key holder is a person I trust and know personally, or that someone I know personally has vouched for their identity.

## Signing process

Steps 1 through 2 are only required for casual and positive certification, steps 3 through 5 are always required.

1. The signee's identity is verified according to the requirements.
2. The signee gives me a physical copy of their key fingerprint, or in the case of verification over video chat, the fingerprint is supplied verbally.
3. The signee sends me a signed email from each email address corresponding to a UID they want signed. 
4. I will reply to each email with the signed key in encrypted form.
5. The signee decrypts the signed keys and optionally publishes them.

## General

I appreciate if anyone whose keys I sign would return the favour and sign my keys in accordance with their own signing policy.

A signed markdown version of this document can be found [here.](policy.md.asc)

## Changelog

2021-11-21: Initial release
